# Dockerfile to build Docker image for building IGB installers via bitbucket pipeline

# About

To build multi-platform IGB installers for users, we use Install4J from [EJ Technologies](https://www.ej-technologies.com).

The installers contain IGB itself together with a Java run-time environment (JRE). We include a JRE so that IGB users do not have to separately maintain Java on their system. Also, we want to make sure that users get the version of Java we are sure will work well with IGB.

## Links

* JRE Bundles maintained by us - https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads
* Docker image - https://hub.docker.com/r/lorainelab/igb-maven-install4j
* IGB Canvas course - https://canvas.instructure.com/courses/1164217 (see DevOps module)

## About the JREs

Prior to JRE 1.8.0_212, we downloaded JRE bundles used by Install4J from [EJ Technologies bundle download site](https://download.ej-technologies.com/bundles/list) and
got Linux bundles from Oracle. When we upgraded to JRE 1.8.0_212, we could not find new JRE bundles at the download site above, so we starting making them using the desktop Install4J application.

Now, when we need to use a new build or version of a JRE, we use the Install4J desktop program to make the new JRE bundles. 

## How to make new JREs for the Docker image

Start by cloning the IGB code base onto your computer. Next, get a copy of Install4J desktop client and install it. Depending on whether you installed a trial version, you may have to enter a license key to continue using the software. Ask a member of the core IGB development team for help if yes.

Within Install4J, do the following:

* Select "Open Project" and chose an install4J configuration file within the distribution directory of the IGB project. Note that if you are updating Install4J, you should open the next highest version file. Install4J will likely try to update the file. Save the project updated file to a different file name, following our project's naming convention.
* Under General Settings > JRE Bundles
	* Configure JDK release section:
		* Set JDK release to "Zulu" 
		* Click the “...” and select the JRE version
			* As of Jan. 2024, we are using 21/21.0.2
	* Configure "Selected additional modules" section:
		* Delete entry "Common JRE modules without JavaFX" if present
		* Add desired "additional module" by clicking the green “+” on the right side of the screen to open new window "Define Module Entry" 
			* Select Entry Type > Default JDK modules 
			* Select Detail > All Modules
			* Observe that the only entry now is "Module set All modules"
* Under Media
	* Double click on Mac_OSX_universal
		* In Installer options, set the Architecture to Universal binaries
		* In Bundled JRE, select Generate a JRE bundle
	* Double click on Windows_x86_64
		* In Bundled JRE, select Generate a JRE bundle
	* Double click on Unix Installer
		* In Bundled JRE, select Generate a JRE bundle
* Under Build
	* In Build selected: select Build all
	* Click Start Build
	* Watch the output printed to the Install4J status log. You may need to inspect the output to figure out where the JRE bundle gets saved on your system.
* Navigate to wherever the JRE tars were saved
	* On Mac, navigate to /Users/USERNAME/Library/Caches/install4j/v10/cached_jres/generated/
	* Look for recently created folders, they should contain the JRE bundles:
		* macos-universal-21.0.2.tar.gz
		* windows-amd64-21.0.2.tar.gz
		* linux-amd64-21.0.2.tar.gz
* Upload the tars to https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads or other publicly accessible location.

## How to update the Docker image

Review the information above on how we use Install4J desktop client to create new JRE's for the Docker image. 

To modify the JRE being used in the Docker container, do this:

* Create new JRE bundles (see above) and upload them to the https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads. 
* Update the Install4J configuration file in IGB project to use the new JRE bundles. The Install4J file is in the IGB project "distribution" folder.
* Update Dockerfile (version controllled here) and rebuild the image. Update image tag to indicate JRE version *and* the Install4J version.

To modify the Install4J version used:

Update the Docker file. Do not include the Install4J license key or any passwords within the image. These data get passed in via environment variables. For more details on that, see the IGB project POM files.

Note that the Docker build process downloads the Install4J file from EJ Technologies. Also, each version of Install4J expects the JRE bundles to reside a location specific to it. Look carefully at the Dockerfile to understand how this works.

After running `docker build` and confirming that the container behaves as expected, push it to our Docker repository, giving it a tag to indicate the versions of Install4J and java it contains. 

For example, image tag I4J8-jre-1.8.0_241 indicates that the image contains:

* Install4J (I4J) version 8
* JRE version 1.8.0 patch number 241

Also, you'll need to update IGB's Bitbucket pipelines YML file to use the new Docker image. 

Review the commit history for this repository and associated Jira tickets for insights on how to do the above.

# Test the Docker image(s) outside of bitbucket pipelines 

Please do not test and debug the new Docker image using Bitbucket
resources. Instead, follow instructions laid out in the IGB Canvas
course at https://canvas.instructure.com/courses/1164217 

Instructions are in a page titled "IGB Docker image build environment"
or similar in the "DevOps" module. (Note that we might change the
title of the page later. If you don't find a page with exactly this
title, please look again.)

Once you have successfully built IGB installers on a Docker host, as
described in the Canvas course, then try it out in bitbucket pipelines.
To do that, make a new branch in an IGB fork and edit the first line of
the bitbucket pipelines configure file. Be sure to run each of the pipeline
options to ensure everything is working as it should. 