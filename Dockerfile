#
# IGB Dockerfile
# Builds image used to build IGB installers
# via Bitbucket pipelines.
#
# Requires JREs for Linux, Windows, MacOS
# we add these to the installer for convenience of
# users so that they don't have to separately install
# Java JRE that works best with IGB.
#
# Other relevant files include:
#
#  - install4J configuration files in distribution directory of
#    IGB project
#  - bitbucket pipelines YML configuration file in top level of
#    IGB project directory
#  - README.md in this repository
#
# Stage 1: Build dependencies and SDKMAN
FROM ubuntu:jammy AS builder

# Install required packages
RUN apt-get update && apt-get install -y \
    curl \
    zip \
    unzip

# Install SDKMAN
RUN curl -s "https://get.sdkman.io" | bash

# Install Java and Maven via SDKMAN
SHELL ["/bin/bash", "-c"]
RUN source "$HOME/.sdkman/bin/sdkman-init.sh" && \
    sdk install java 17.0.8.fx-zulu && \
    sdk install java 21.0.2.fx-zulu && \
    sdk install maven 3.9.6

# Stage 2: Final image with JREs and Maven
FROM ubuntu:jammy

# Install required packages
RUN apt-get update && apt-get install -y \
    curl \
    zip \
    unzip \ 
    fontconfig

# Copy JDK and Maven from the builder stage
COPY --from=builder /root/.sdkman /root/.sdkman

# Set environment variables for JDK and Maven
ENV JAVA_HOME=/root/.sdkman/candidates/java/current
ENV MAVEN_HOME=/root/.sdkman/candidates/maven/current
ENV PATH=$JAVA_HOME/bin:$MAVEN_HOME/bin:$PATH

COPY settings.xml /root/.m2/settings.xml

# Install Install4J (required for mvn install4j plugin)
RUN curl -o install4j.deb https://download.ej-technologies.com/install4j/install4j_linux-x64_10_0_6.deb && \
    dpkg -i install4j.deb && \
    rm install4j.deb

# Get JRE bundles that will be packaged with IGB
RUN curl -Lo windows-amd64-21.0.2.tar.gz https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/windows-amd64-21.0.2.tar.gz && \
    curl -Lo macos-universal-21.0.2.tar.gz https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/macos-universal-21.0.2.tar.gz && \
    curl -Lo linux-amd64-21.0.2.tar.gz https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/linux-amd64-21.0.2.tar.gz

# Move jre tarballs to location install4j v10 expects them to reside
RUN mkdir -p /root/.local/share/install4j/v10/jres/ && \
    cp *21.0.2.tar.gz /root/.local/share/install4j/v10/jres/ && \
    rm *.tar.gz

# Add the INSTALL4J_JAVA_HOME environment variable
ENV INSTALL4J_JAVA_HOME=/root/.sdkman/candidates/java/17.0.8.fx-zulu
ENV INSTALL4J_HOME=/opt/install4j10

# Remove to force install4j to use INSTALL4J_JAVA_HOME as it won't without removing its jre folder
RUN rm -rf /opt/install4j10/jre

CMD ["mvn"]
